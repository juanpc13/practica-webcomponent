class BuscadorMarca extends HTMLElement{
  constructor() {
    super();
    this.root = this.attachShadow({mode:'closed'});
  }

  connectedCallback(){
    console.log(this.array);
    this.titulo = document.createElement("H1");
    this.titulo.appendChild(document.createTextNode("Autocomplete"))
    this.entrada = document.createElement("INPUT");
    this.entrada.setAttribute("type", "text");
    this.data = document.createElement("DATALIST");
    this.data.setAttribute("id", "myDatalist");
    this.t = document.createElement("OPTION");
    this.t.appendChild(document.createTextNode("J y D"));
    this.data.appendChild(this.t);
    this.entrada.setAttribute("list", "myDatalist");
    this.root.appendChild(this.titulo);
    this.root.appendChild(this.entrada);
    this.root.appendChild(this.data);
  }
}
customElements.define("marca-finder", BuscadorMarca);
