import AbstractResourceClient from './AbstractResourceClient.js'
class MarcaResourceClient extends AbstractResourceClient {
  constructor() {
    super();
    this.url+="/marcas";
  }

  findByName(nombre){
    return fetch(url+"/findbyname/"+nombre);
  }

}
export default MarcaResourceClient;
